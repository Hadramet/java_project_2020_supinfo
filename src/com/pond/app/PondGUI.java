package com.pond.app;

import javax.swing.*;
import java.awt.*;

public class PondGUI  {

    private  JFrame frame ;

    public PondGUI(JComponent component, String name) {

        frame = new JFrame(name);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.add(component);
        frame.pack();
    }

    public void showComponent()
    {
        EventQueue.invokeLater(() -> {
            try {
                UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
            } catch (ClassNotFoundException |
                    InstantiationException |
                    IllegalAccessException |
                    UnsupportedLookAndFeelException e)
            {
                e.printStackTrace();
            }
            frame.setVisible(true);
            frame.setResizable(false);
        });

        //animate();
    }
}
