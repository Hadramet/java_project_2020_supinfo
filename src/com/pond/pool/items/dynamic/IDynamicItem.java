package com.pond.pool.items.dynamic;

import com.pond.pool.items.IPoolItem;

import java.awt.Point;

public interface IDynamicItem extends IPoolItem {

    // TODO : Method for static items here
    void setDynamicPosition(Point position) ;

}
