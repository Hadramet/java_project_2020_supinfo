package com.pond.pool.items.dynamic;

import java.awt.*;

public class Rock implements IDynamicItem {

    private Rectangle shape;
    private Color color;

    public Rock() {
        color = Color.GRAY;
        int x = (int) (Math.random() * 750);
        int y = (int) (Math.random() * 350);
        shape = new Rectangle(new Dimension(15,15));
        setDynamicPosition(new Point(x,y));
    }

    @Override
    public void setDynamicPosition(Point position) {
        this.shape.x = (position.x - shape.width / 2 );
        this.shape.y = (position.y - shape.width / 2 );
    }

    @Override
    public void drawItem(Graphics graphics) {
        graphics.setColor(color);
        graphics.fillRect(shape.x, shape.y, shape.height,shape.width);
    }

    @Override
    public Rectangle getInstance() {
        return (Rectangle) shape.clone();
    }
}
