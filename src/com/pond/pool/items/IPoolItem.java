package com.pond.pool.items;

import java.awt.*;

public interface IPoolItem {

    void drawItem(Graphics graphics);
    Rectangle getInstance() ;

}
