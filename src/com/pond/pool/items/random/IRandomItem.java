package com.pond.pool.items.random;

import com.pond.pool.items.IPoolItem;

import java.awt.Point;

public interface IRandomItem extends IPoolItem {

    // Methods for random items here
    void setPosition (Point position) ;

}
