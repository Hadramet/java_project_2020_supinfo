package com.pond.pool.items.random.movable;

import java.awt.*;

public class Duck extends AMovableItems
{

    protected static final int MAX_EAT = 15 ;
    protected int has_eat = 0;
    protected int eat_point = 250 ;

    public Duck()
    {
        super(Color.MAGENTA, new Dimension(10,10) );
    }
    public Duck(Point position) { super(position);}


    // Eating action
    public void eat()
    {
        has_eat++;
        if (!(this instanceof HeadDuck))
            this.setDimension();
        if(life < MAX_LIFE ) life += eat_point ;
    }

    // SetDimension
    public void setDimension ()
    {
        this.shape.setSize(this.shape.width+1,this.shape.height+1);
    }


    // Check if Duck can be HeadDuck
    public Boolean canBeHead(){
        return has_eat == MAX_EAT;
    }

    @Override
    public void drawItem(Graphics graphics) {
        graphics.setColor(color);
        graphics.fillRect(shape.x, shape.y, shape.height,shape.width);
    }

}
