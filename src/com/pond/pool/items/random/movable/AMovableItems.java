package com.pond.pool.items.random.movable;

import com.pond.pool.items.random.IRandomItem;

import java.awt.*;

public abstract class AMovableItems implements IRandomItem
{
    private static final int MAX_WIDTH = 800 ;
    private static final int MAX_HEIGHT = 400 ;
    private static final int MIN_WIDTH = 5 ;
    private static final int MIN_HEIGHT = 5 ;
    protected static int MAX_LIFE = 1000;
    private static final int counter = 50;

    private enum MOVE {
        UP, DOWN, RIGHT, LEFT ;

        public static MOVE getRandom() {
            return values()[(int) (Math.random() * values().length)];
        }
    }

    private static int  direction_counter = counter ;
    protected MOVE direction = MOVE.DOWN ;
    protected int life = MAX_LIFE ;

    public int getLife() {
        return life;
    }

    protected Rectangle shape ;
    protected Color color ;

    // Constructor
    public AMovableItems ( Color color, Dimension dimension)
    {
        int x = (int) (Math.random() * 750);
        int y = (int) (Math.random() * 350);
        this.color = color ;
        this.shape = new Rectangle(dimension);
        setPosition(new Point(x,y));
    }
    public AMovableItems(Point position) {
        this.shape = new Rectangle(new Dimension(35,35));
        this.setPosition(position);
    }

    // Abstract method to draw items
    public abstract void drawItem(Graphics graphics);


    // Attribution de direction
    public void setDirection(MOVE move)
    {
        direction = move;
        direction_counter = counter;
    }



    // Methods to move
    public void move(Boolean collision) {


        // Pour changer les directions
        if (direction_counter == 0 )
        {
            setDirection(MOVE.getRandom());
        }

        // Gestion des collisions
        if (!collision)
        {
            if (shape.x >= MAX_WIDTH - shape.width ) setDirection(MOVE.LEFT);
            if (shape.x <= MIN_WIDTH ) setDirection(MOVE.RIGHT);
            if (shape.y >= MAX_HEIGHT - shape.width) setDirection(MOVE.UP) ;
            if (shape.y <= MIN_HEIGHT) setDirection(MOVE.DOWN);

        }else {
            setDirection(MOVE.getRandom());
        }

        // Incrementation ou decrementation des
        // attributs en fonction de la direction
        switch (direction){
            case RIGHT:
                shape.x++;
                direction_counter--;
                break;
            case LEFT:
                shape.x--;
                direction_counter--;
                break;
            case DOWN:
                shape.y++;
                direction_counter--;
                break;
            case UP:
                shape.y--;
                direction_counter--;
                break;
        }

        life -- ;
    }

    @Override
    public Rectangle getInstance() {
        return (Rectangle) shape.clone();
    }

    // Override setPosition of interface RandomItem
    @Override
    public void setPosition(Point position)
    {
        this.shape.x = (position.x - shape.width / 2 );
        this.shape.y = (position.y - shape.width / 2 );
    }

}
