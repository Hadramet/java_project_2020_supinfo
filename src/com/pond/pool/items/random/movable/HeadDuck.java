package com.pond.pool.items.random.movable;

import java.awt.*;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class HeadDuck extends Duck {

    private ArrayList<Duck> duckSquad = new ArrayList<>();

    public HeadDuck(Point position)
    {
        super(position);
        duckSquad.add(new Duck());
        duckSquad.add(new Duck());
    }

    public void setParameter()
    {
        this.color = Color.RED;
        this.shape.setSize(MAX_EAT+10,MAX_EAT+10);
        MAX_LIFE = 5000 ;
        eat_point = 500 ;
    }

    public void addDuckToSquad(Duck duck) {
        duckSquad.add(duck) ;
    }

    // Methode de whistle
    public void whistle () {
        System.out.println(this.getInstance() + " whistle.........POOhhh PPOOOHHhhh");
    }

    @Override
    public void drawItem(Graphics g) {
        super.drawItem(g);
        Graphics2D g2d = (Graphics2D) g.create();
        for (Duck duck : duckSquad) {
            duck.drawItem(g2d);
        }
        g2d.dispose();
    }
}
