package com.pond.pool.items.random.edible;

import javafx.scene.shape.Circle;

import java.awt.*;

public class WaterLilie implements IEdibleItem {

    private Circle shape ;
    private Color color;


    public WaterLilie() {
        color = Color.GREEN ;
        float x = (float) (Math.random() * 750);
        float y = (float) (Math.random() * 350);
        shape = new Circle(x, y,10) ;
    }

    @Override
    public void drawItem(Graphics graphics) {

        graphics.setColor(color);
        graphics.fillOval((int)shape.getCenterX(),(int) shape.getCenterY(),10,10);
    }

    @Override
    public Circle getShape()
    {
        return shape;
    }

    @Override
    public Rectangle getInstance() {
        return null ;
    }

    @Override
    public void setPosition(Point position) {
        // TODO
    }
}
