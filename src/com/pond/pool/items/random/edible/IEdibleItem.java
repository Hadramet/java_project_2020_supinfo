package com.pond.pool.items.random.edible;

import com.pond.pool.items.IPoolItem;
import com.pond.pool.items.random.IRandomItem;
import javafx.scene.shape.Circle;

public interface IEdibleItem extends IRandomItem {
    Circle getShape();
}
