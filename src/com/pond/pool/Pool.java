package com.pond.pool;

import com.pond.pool.items.IPoolItem;
import com.pond.pool.items.dynamic.Rock;
import com.pond.pool.items.random.edible.IEdibleItem;
import com.pond.pool.items.random.edible.WaterLilie;
import com.pond.pool.items.random.movable.AMovableItems;
import com.pond.pool.items.random.movable.Duck;
import com.pond.pool.items.random.movable.HeadDuck;

import javax.swing.*;
import java.awt.*;
import java.util.LinkedList;

public class Pool extends JPanel
{

    private static final int MAX_EDIBLE = 50 ;
    private static final int MAX_DUCK = 3 ;
    private static final int MAX_ROCK = 3 ;


    private LinkedList<IPoolItem> poolItems = new LinkedList<>() ;
    private int edibleNb = 0 ;

    // Constructor
    public Pool ()
    {
        addRandomMovableItem();
        addRandomDynamicItem();
    }

    public void checkDuckNb () {
        int i = 0 ;
        for (IPoolItem item : poolItems){
            if (item instanceof AMovableItems) i++ ;
        }
        if (i< MAX_DUCK) addRandomMovableItem();
    }

    // Add item to poolItems list
    public void addItem(IPoolItem item)
    {
        poolItems.add(item);
        repaint();
    }

    // Remove Item in poolItems list
    private void removeItem(IPoolItem item)
    {
        poolItems.remove(item);
        repaint();
    }

    // Add Randomly Movable Items to the poolItems List
    private void addRandomMovableItem() {
        for (int i = 0 ; i <   MAX_DUCK ; i++ ) addItem(new Duck());
    }

    // Add randomly Dynamic Items to the poolItems List
    private void addRandomDynamicItem() {
        for (int i = 0 ; i < MAX_ROCK ; i++ ) this.addItem(new Rock());
    }

    // Add randomly Edible Items to the poolItems List
    public void addRandomEdibleItem()  {
        if ( edibleNb < MAX_EDIBLE )
        {
            addItem(new WaterLilie());
            edibleNb++;
        }
    }

    // remove edible items and decrease edible number
    public void removeEdibleItem(IPoolItem item2) {

        removeItem(item2);
        edibleNb--;

    }

    // Handle action to do if Movable item collide with non Edible Item
    private void movableCollisionNonEdible(IPoolItem item, IPoolItem item2) {

        if (!item2.equals(item) && collisionNonEdible(item,item2))
            ((AMovableItems) item).move(true);

    }

    // Handle action to do if Movable item collide with Edible Item
    private void movableCollisionEdible(IPoolItem item, IPoolItem item2) {

        if (item2 instanceof IEdibleItem && collisionEdible(item2,item))
        {
            // Eat the edible and remove from the pool
            ((Duck) item).eat();
            removeEdibleItem(item2);

            // If duck can be headDuck
            if ( ((Duck) item).canBeHead() && !(item instanceof HeadDuck))
                changeInstance(item);
        }

        // Each time this method is call for headDuck he whistle .
        if (item instanceof HeadDuck)
            ((HeadDuck) item).whistle() ;

    }

    // Move Movable item
    private void movableMovementAction(IPoolItem item) {

        int i = 0;
        while (i < poolItems.size())
        {
            // Collision for changing movement
            var item2 = poolItems.get(i) ;

            //
            System.out.println(" Distance is : " + distanceBetweenItems(item,item2));

            if (item2.getInstance() != null)
                movableCollisionNonEdible(item,item2);
            else
                movableCollisionEdible(item,item2);

            i++;
        }

    }

    // Animate Item if Movable
    public void itemAnimation(IPoolItem item) {

        if (item instanceof AMovableItems)
        {
            if (((AMovableItems) item).getLife() > 0 )
            {
                // Move Action for the movable
                this.movableMovementAction(item);

                // Move whatever
                ((AMovableItems) item).move(false);

                // Update the number off edible in pool
                this.addRandomEdibleItem();
                repaint();
            }
            else {
                this.removeItem(item);
            }

        }

    }

    // Change Edible item (Duck) to (Head Duck)
    private void changeInstance (IPoolItem item)
    {
        if (item instanceof Duck)
        {
            Point position = item.getInstance().getLocation();

            if (poolItems.contains(item)) removeItem(item);
            var new_item = new HeadDuck(position); // Replace by new HeadDuck type at same position.

            new_item.setParameter();
            poolItems.add(new_item);
        }
    }

    // Return poolItems List
    public LinkedList<IPoolItem> getPoolItems()
    {
        return poolItems;
    }


    // Check if Movable item and Edible item collide
    // Source : https://www.gamedevelopment.blog/collision-detection-circles-rectangles-and-polygons/
    boolean collisionEdible(IPoolItem item2, IPoolItem item)
    {
        var circle = ((IEdibleItem) item2).getShape() ;
        var rectangle = item.getInstance() ;

        double distance_x = Math.abs(circle.getCenterX() - rectangle.x);
        double distance_y = Math.abs(circle.getCenterY() - rectangle.y);


        if (distance_x > ((double) (rectangle.width / 2) + circle.getRadius())) return false;
        if (distance_y > ((double) (rectangle.height / 2) + circle.getRadius())) return false;

        if (distance_x <= (double)(rectangle.width/2)) return true;
        if (distance_y <= (double)(rectangle.height/2)) return true;

        double cDist_sq = Math.pow((distance_x - (double) rectangle.width/2),2) +
                            Math.pow((distance_y - (double)rectangle.height/2),2);

        return (cDist_sq <= Math.pow(circle.getRadius(),2));

    }

    // Chek if Movable item collide with another Movable or Dynamic item
    public boolean collisionNonEdible(IPoolItem item, IPoolItem item2)
    {
        return item.getInstance().intersects(item2.getInstance());
    }

    // Calcule distance between two item .
    public double distanceBetweenItems (IPoolItem item, IPoolItem item2)
    {
        if (item2 instanceof AMovableItems && item instanceof AMovableItems)
        {
            return Math.sqrt
                    (
                            Math.pow((item2.getInstance().x - item.getInstance().x),2) +
                                    Math.pow((item2.getInstance().y - item.getInstance().y),2)
                    );
        }
        else return 0.0 ;
    }


    @Override
    public void paint(Graphics g) {
        super.paint(g);
        Graphics2D graphics2D = (Graphics2D) g ;

        for (IPoolItem poolItem : poolItems) {
            poolItem.drawItem(graphics2D);
        }
    }
}
