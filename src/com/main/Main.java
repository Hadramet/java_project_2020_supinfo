package com.main;

import com.pond.app.PondGUI;
import com.pond.pool.Pool;
import com.pond.pool.items.random.movable.AMovableItems;

import javax.swing.*;
import java.awt.*;
public class Main {
    public static void main(String[] args) {
        System.out.println("Hello world");
        Pool pool = new Pool();

        pool.setBackground(Color.BLACK);
        pool.setPreferredSize(new Dimension(800,400));

        // Show pool on the pond gui
        PondGUI visual =  new PondGUI(pool,"pool");

        // Timer
        Timer timer = null;

        try {
            timer = new Timer(10, actionEvent ->
            {
                for(int i = 0; i < pool.getPoolItems().size();i++)
                {
                    if (pool.getPoolItems().get(i) instanceof AMovableItems)
                    {
                       pool.itemAnimation(pool.getPoolItems().get(i));
                        pool.checkDuckNb();
                    }

                }
                // updateDuck


            });
        } catch (Exception e) {

            e.printStackTrace();
        }


        visual.showComponent();

        assert timer != null;
        timer.setRepeats(true);
        timer.setCoalesce(true);
        timer.start();

    }
}
